#include "Console.h"
#include "Logger.h"
#include "Loader.h"
#include "Element.h"
#include <fstream>
#include <sstream>

Console::Console() {}

Console::~Console() {
    // already terminated before
    //terminate();
}

void Console::start(){
    askLoadFromFileVerticesIndices();
    askOptions();
    //askSaveToFile();
}

bool Console::askLoadSegment(){
    std::stringstream convert;
    std::string line;
    for (unsigned int i = 0; i < 2; i++){
        std::cout << "Segment point " << (i + 1) << "\n";
        std::cout << "Insert x and y separated by at least one space: ";
        std::getline(std::cin, line);
        float x, y;
        convert.str(line);
        convert >> x >> y;
        if (convert.fail()){
            LOG(LogLevel::ERROR) << "problems when reading segment, please insert two numbers separated by at least one space";
            app.clearSegment();
            return false;
        }
        convert.clear();
        app.addSegmentPoint(x, y);
    }
    LOG(LogLevel::INFO) << "Segment loaded correctly";
    return true;
}

void Console::askLoadFromFileVerticesIndices(){
    std::string consoleString;

    bool continueLoop = true;

    while (continueLoop){
        continueLoop = false;
        std::cout << "Enter the name of the file you what to load polygon from: ";
        std::getline(std::cin, fileName);
        fileName = "files/polygons/" + fileName;
        int numberVertices = Loader::GetNumberVerticesFromFile(fileName);
        if (numberVertices <= 0){
            continueLoop = true;
            continue;
        }
        if (Loader::SearchInFile(fileName, "vertices") <= 0){
            continueLoop = true;
            continue;
        } else{
            LOG(LogLevel::INFO) << "Vertices found";
        }
        if (Loader::SearchInFile(fileName, "indices", false) > 0){
            LOG(LogLevel::INFO) << "Indices found";
            std::cout << "Do you want to load indices? (If not it is assumed that vertices are in order) [Y/n] ";
            std::getline(std::cin, consoleString);
            if (consoleString == "n" || consoleString == "N"){
                if (Loader::LoadVerticesIndicesFromFile(app.getVerticesForLoading(), app.getIndicesForLoading(),
                                                 fileName, numberVertices, false) < 0){
                    continueLoop = true;
                    continue;
                }
            } else{
                if (Loader::LoadVerticesIndicesFromFile(app.getVerticesForLoading(), app.getIndicesForLoading(),
                                                 fileName, numberVertices, true) < 0){
                    continueLoop = true;
                    continue;
                }
            }
        } else{
            LOG(LogLevel::INFO) << "Indices not found, it is assumed that vertices are in order";
            if (Loader::LoadVerticesIndicesFromFile(app.getVerticesForLoading(), app.getIndicesForLoading(),
                                             fileName, numberVertices, false) < 0){
                continueLoop = true;
                continue;
            }
        }
    }
    LOG::NewLine();
}

void Console::askLoadFromFileSegment(){
    std::string consoleString;
    if (Loader::SearchInFile(fileName, "segment", false) > 0){
        LOG(LogLevel::INFO) << "Segment found";
        bool continueSecondLoop = true;
        while (continueSecondLoop){
            continueSecondLoop = false;
            std::cout << "Do you want to load segment? (If not you can insert it manually) [Y/n] ";
            std::getline(std::cin, consoleString);
            if (consoleString == "n" || consoleString == "N"){
                if (!askLoadSegment()){
                    continueSecondLoop = true;
                    continue;
                }
            } else{
                if (Loader::LoadSegmentFromFile(app.getSegmentForLoading(), fileName) < 0){
                    LOG(LogLevel::WARN) << "Segment not loaded";
                    app.clearSegment();
                    continueSecondLoop = true;
                    continue;
                }
            }
        }
    } else{
        LOG(LogLevel::INFO) << "Segment not found, you have to insert it manually";
        while (!askLoadSegment());
    }
}

void Console::askOptions(){
    std::string consoleString;

    bool continueLoop = true;

    while (continueLoop){
        continueLoop = false;
        std::cout << "Press 'c' to cut polygon, 'e' to create element: ";
        std::getline(std::cin, consoleString);
        if (consoleString == "c" || consoleString == "C"){
            createCutPolygon();
        } else if (consoleString == "e" || consoleString == "E"){
            createElement();
        } else{
            LOG(LogLevel::WARN) << "You can only press 'c' ('C') or 'e' ('E')";
            continueLoop = true;
        }
    }
}

void Console::createCutPolygon(){
    askLoadFromFileSegment();

    app.createMainPolygon();
    app.cutMainPolygon();

    LOG::NewLine(LogLevel::INFO);
    app.getPolygon().printVertices();

    const std::vector<std::shared_ptr<std::vector<unsigned int>>>& polygonsIndices = app.getPolygonsIndices();
    LOG::NewLine(LogLevel::INFO);
    LOG(LogLevel::INFO) << "Number of polygons: " << polygonsIndices.size();
    for (unsigned int i = 0; i < polygonsIndices.size(); i++){
        const std::vector<unsigned int>& indices = *polygonsIndices[i];
        {
            LOG log = LOG(LogLevel::INFO);
            log << "Indices small polygon " << i << ": ";
            for (unsigned int n = 0; n < indices.size(); n++){
                log << indices[n] << " ";
            }
        }
    }
}

void Console::createElement(){
    app.createMainPolygon();
    Element element = Element(app.getPolygon());
    element.createElement();
}
