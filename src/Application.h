#ifndef APPLICATION_H
#define APPLICATION_H

#include "Polygon.h"
#include "Vector2f.h"
#include <vector>
#include <iostream>
#include <memory>


class Application{

public:
    Application();

    Application(const Application&)                 = delete;
    Application(Application&&) noexcept             = delete;
    Application& operator=(const Application&)      = delete;
    Application& operator=(Application&&) noexcept  = delete;

    const std::vector<unsigned int>& getIndices() const;
    const std::vector<Vector2f>& getVertices() const;
    unsigned int getNumberVertices() const;

    const std::vector<Vector2f>& getSegmentPoints() const;
    unsigned int getSegmentSize() const;
    void addSegmentPoint(float x, float y);
    void clearSegment();

    const Polygon& getPolygon() const;
    void createMainPolygon();
    void cutMainPolygon();
    const std::vector<std::shared_ptr<std::vector<unsigned int>>>& getPolygonsIndices() const;

    void printVertices() const;
    void printIndices() const;

    void clear();

    // used when loading from file
    std::vector<unsigned int>& getIndicesForLoading();
    std::vector<Vector2f>& getVerticesForLoading();
    std::vector<Vector2f>& getSegmentForLoading();

private:
    std::vector<Vector2f> vertices;
    std::vector<unsigned int> indices;
    Polygon mainPolygon;
    std::vector<Vector2f> segmentPoints;
    std::vector<std::shared_ptr<std::vector<unsigned int>>> polygonsIndices;
};

#endif // APPLICATION_H
