#ifndef CONSOLE_H
#define CONSOLE_H

#include "Application.h"


class Console{

public:
    Console();
    ~Console();

    Console(const Console&)                 = delete;
    Console(Console&&) noexcept             = delete;
    Console& operator=(const Console&)      = delete;
    Console& operator=(Console&&) noexcept  = delete;

    void start();

private:
    Application app;

    std::string fileName;

    bool askLoadSegment();

    void askLoadFromFileVerticesIndices();
    void askLoadFromFileSegment();

    void askOptions();

    void createCutPolygon();

    void createElement();
};

#endif // CONSOLE_H
