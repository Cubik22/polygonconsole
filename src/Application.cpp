#include "Application.h"
#include "Intersector.h"
#include "Logger.h"
#include <fstream>
#include <sstream>

Application::Application() {}

const std::vector<unsigned int>& Application::getIndices() const{
    return indices;
}

const std::vector<Vector2f>& Application::getVertices() const{
    return vertices;
}

unsigned int Application::getNumberVertices() const{
    return vertices.size();
}

const std::vector<Vector2f>& Application::getSegmentPoints() const{
    return segmentPoints;
}

unsigned int Application::getSegmentSize() const{
    return segmentPoints.size();
}

void Application::addSegmentPoint(float x, float y){
    segmentPoints.emplace_back(x, y);
}

void Application::clearSegment(){
    segmentPoints.clear();
}

const Polygon& Application::getPolygon() const{
    return mainPolygon;
}

void Application::createMainPolygon(){
    mainPolygon.setBody(vertices, indices);
}


void Application::cutMainPolygon(){
    mainPolygon.setSegment(segmentPoints[0], segmentPoints[1]);
    mainPolygon.createNetwork();
    //Node::PrintNetwork(mainPolygon.getStartNode());
    polygonsIndices = mainPolygon.cut();
    LOG(LogLevel::INFO) << "End of cutting";
}

const std::vector<std::shared_ptr<std::vector<unsigned int>>>& Application::getPolygonsIndices() const{
    return polygonsIndices;
}

void Application::printVertices() const{
    LOG(LogLevel::INFO) << "Printing application vertices";
    for (unsigned int i = 0; i < vertices.size(); i++){
        LOG(LogLevel::INFO) << i << " x: " << vertices[i].x << " y: " << vertices[i].y;
    }
}

void Application::printIndices() const{
    LOG(LogLevel::INFO) << "Printing application indices";
    for (unsigned int i = 0; i < indices.size(); i++){
        LOG(LogLevel::INFO) << i << " " << indices[i];
    }
}

void Application::clear(){
    vertices.clear();
    indices.clear();
    //mainPolygon = Polygon();
    mainPolygon.deleteStartNode();
    polygonsIndices.clear();
    segmentPoints.clear();
    LOG(LogLevel::DEBUG) << "Application cleared";
}

std::vector<unsigned int>& Application::getIndicesForLoading(){
    return indices;
}

std::vector<Vector2f>& Application::getVerticesForLoading(){
    return vertices;
}

std::vector<Vector2f>& Application::getSegmentForLoading(){
    return segmentPoints;
}
