#define UNIT_TEST

#ifndef UNIT_TEST

#include "Console.h"
#include "Logger.h"

#else

#include "test/TestVector2f.h"
#include "test/TestIntersector.h"
#include "test/TestPolygon.h"
#include "test/TestElement.h"
#include "test/TestMesh.h"

#endif

int main(
#ifdef UNIT_TEST
         int argc, char** argv
#endif
){

#ifndef UNIT_TEST

    LOG::LEVEL = LogLevel::INFO;

    LOG::PRINT_INFO_LEVEL = false;

    Console console;
    console.start();
    return 0;

#else

    LOG::LEVEL = LogLevel::ERROR;

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

#endif
}

