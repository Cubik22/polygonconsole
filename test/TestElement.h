#ifndef TESTELEMENT_H
#define TESTELEMENT_H

#include "UtilsTest.h"


namespace test {

TEST(TestElement, Load){
    std::vector<Vector2f> startVertices;
    std::vector<unsigned int> startIndices;
    std::vector<Vector2f> startSegment;
    std::vector<std::shared_ptr<std::vector<unsigned int>>> startSolygonsIndices;
    ASSERT_TRUE(LoadPolygonFromFile(startVertices, startIndices, startSegment, startSolygonsIndices, "polygons/" + GetPolygonsFileNames()[0]));

    Polygon poly = Polygon(startVertices, startIndices);
    Element element = Element(poly);

    SmartVectorEqual(startVertices, element.getPoints());
    EXPECT_EQ(startIndices, element.getStartingIndices());
}

TEST(TestElement, Create){
    const std::vector<std::string> fileNames = GetElementsFileNames();

    for (const std::string& fileName : fileNames){
        std::vector<Vector2f> startVertices;
        std::vector<unsigned int> startIndices;
        std::vector<Vector2f> startSegment;
        std::vector<std::shared_ptr<std::vector<unsigned int>>> startSolygonsIndices;
        ASSERT_TRUE(LoadPolygonFromFile(startVertices, startIndices, startSegment, startSolygonsIndices, "polygons/" + fileName));

        Polygon poly = Polygon(startVertices, startIndices);
        Element element = Element(poly);
        element.createElement();

        std::vector<Vector2f> correctVertices;
        std::vector<std::shared_ptr<std::vector<unsigned int>>> correctIndices;
        ASSERT_TRUE(LoadElementFromFile(correctVertices, correctIndices, "elements/" + fileName));

        SmartVectorEqual(correctVertices, element.getPoints());

        SmartVectorEqual(correctIndices, element.getPolygonsIndices());
    }
}

}

#endif // TESTELEMENT_H
