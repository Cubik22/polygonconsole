#include "UtilsTest.h"

const std::vector<std::string>& test::GetPolygonsFileNames(){
    static const std::vector<std::string> polygonsFileNames = {
        "base.txt", "easy.txt", "first.txt", "rec.txt", "tri.txt", "snake.txt", "medium.txt", "hard.txt"
    };
    return polygonsFileNames;
}

const std::vector<std::string>& test::GetPolygonsUnitTestsFileNames(){
    static const std::vector<std::string> polygonsUnitTestsFileNames = {
        "rectangle.txt", "convex.txt", "concave.txt"
    };
    return polygonsUnitTestsFileNames;
}

const std::vector<std::string>& test::GetElementsFileNames(){
    static const std::vector<std::string> elementFileNames = {
        "base.txt", "easy.txt", "first.txt", "tri.txt", "snake.txt", "medium.txt"
    };
    return elementFileNames;
}

bool test::IsTestHard(const std::string& name){
    if (name == "medium.txt" || name == "hard.txt"){
        return true;
    }
    return false;
}

std::vector<std::vector<Vector2f>> test::GetVerticesBorders(float width, float height,
                                                                               float borderThickX, float borderThickY){
    float halfWidth  = width  / 2.0f;
    float halfHeight = height / 2.0f;

    float quarterWidth  = halfWidth  / 2.0f;
    float quarterHeight = halfHeight / 2.0f;

//    float eightWidth  = quarterWidth  / 2.0f;
    float eightHeight = quarterHeight / 2.0f;

    return {
        {   {halfWidth, 0.0f},
            {width - borderThickX, halfHeight},
            {halfWidth, height - borderThickY},
            {0.0f, halfHeight}
        },
        {   {quarterWidth, 0.0f},
            {halfWidth, halfHeight - eightHeight},
            {width - quarterWidth, eightHeight},
            {width - borderThickX, halfHeight},
            {width - quarterWidth, height - borderThickY},
            {halfWidth, halfHeight + eightHeight},
            {quarterWidth, height - eightHeight},
            {0.0f, halfHeight}
        }
    };
}

bool test::AreDoubleEqualPercentage(double first, double second, double percentage){
    if (abs(first - second) / abs(first) < abs(percentage)){
        return true;
    }
    return false;
}

bool test::LoadVerticesIndicesFromFile(std::vector<Vector2f>& vertices, std::vector<unsigned int>& indices, const std::string& _fileName){
    const std::string fileName = "files/" + _fileName;
    int numberVertices = Loader::GetNumberVerticesFromFile(fileName);
    if (numberVertices <= 0){
        return false;
    }
    if (Loader::SearchInFile(fileName, "vertices") <= 0){
        return false;
    } else{
        LOG(LogLevel::INFO) << "Vertices found";
    }
    if (Loader::SearchInFile(fileName, "indices", false) > 0){
        LOG(LogLevel::INFO) << "Indices found";
        if (Loader::LoadVerticesIndicesFromFile(vertices, indices, fileName, numberVertices, true) < 0){
            return false;
        }
    } else{
        LOG(LogLevel::INFO) << "Indices not found, it is assumed that vertices are in order";
        if (Loader::LoadVerticesIndicesFromFile(vertices, indices, fileName, numberVertices, false) < 0){
            return false;
        }
    }
    return true;
}

bool test::LoadPolygonFromFile(std::vector<Vector2f>& vertices, std::vector<unsigned int>& indices,
                               std::vector<Vector2f>& segment,
                               std::vector<std::shared_ptr<std::vector<unsigned int>>>& polygonsIndices,
                               const std::string& _fileName){
    const std::string fileName = "files/" + _fileName;
    int numberVertices = Loader::GetNumberVerticesFromFile(fileName);
    if (numberVertices <= 0){
        return false;
    }
    if (Loader::SearchInFile(fileName, "vertices") <= 0){
        return false;
    } else{
        LOG(LogLevel::INFO) << "Vertices found";
    }
    if (Loader::SearchInFile(fileName, "indices", false) > 0){
        LOG(LogLevel::INFO) << "Indices found";
        if (Loader::LoadVerticesIndicesFromFile(vertices, indices, fileName, numberVertices, true) < 0){
            return false;
        }
    } else{
        LOG(LogLevel::INFO) << "Indices not found, it is assumed that vertices are in order";
        if (Loader::LoadVerticesIndicesFromFile(vertices, indices, fileName, numberVertices, false) < 0){
            return false;
        }
    }
    if (Loader::SearchInFile(fileName, "segment", false) > 0){
        LOG(LogLevel::INFO) << "Segment found";
        if (Loader::LoadSegmentFromFile(segment, fileName) < 0){
            LOG(LogLevel::ERROR) << "Segment not loaded";
            return false;
        }
    } else{
        LOG(LogLevel::ERROR) << "Segment not found";
        return false;
    }
    int numberSmallPolygonsIndices = Loader::GetNumberSmallPolygonsFromFile(fileName);
    if (numberVertices <= 0){
        return false;
    }
    if (Loader::SearchInFile(fileName, "small polygons indices") <= 0){
        return false;
    } else{
        LOG(LogLevel::INFO) << "Small polygons indices found";
    }
    if (Loader::LoadSmallPolygonsIndicesFromFile(polygonsIndices, fileName, numberSmallPolygonsIndices) < 0){
        return false;
    }
    return true;
}

bool test::LoadElementFromFile(std::vector<Vector2f>& vertices,
                               std::vector<std::shared_ptr<std::vector<unsigned int> > >& polygonsIndices,
                               const std::string& _fileName){
    const std::string fileName = "files/" + _fileName;
    int numberVertices = Loader::GetNumberVerticesFromFile(fileName);
    if (numberVertices <= 0){
        return false;
    }
    if (Loader::SearchInFile(fileName, "vertices") <= 0){
        return false;
    } else{
        LOG(LogLevel::INFO) << "Vertices found";
        Loader::LoadJustVerticesFromFile(vertices, fileName, numberVertices);
    }
    int numberSmallPolygonsIndices = Loader::GetNumberSmallPolygonsFromFile(fileName);
    if (numberVertices <= 0){
        return false;
    }
    if (Loader::SearchInFile(fileName, "small polygons indices") <= 0){
        return false;
    } else{
        LOG(LogLevel::INFO) << "Small polygons indices found";
    }
    if (Loader::LoadSmallPolygonsIndicesFromFile(polygonsIndices, fileName, numberSmallPolygonsIndices) < 0){
        return false;
    }
    return true;
}

void test::SmartVectorEqual(const std::vector<Vector2f>& first, const std::vector<Vector2f>& second){
    unsigned int sizeFirst  = first.size();
    unsigned int sizeSecond = second.size();

    ASSERT_TRUE(sizeFirst  > 0);
    ASSERT_TRUE(sizeSecond > 0);
    ASSERT_EQ(sizeFirst, sizeSecond);

    for (unsigned int i = 0; i < sizeFirst; i++){
        bool found = false;
        for (unsigned int l = 0; l < sizeSecond; l++){
            if (first[i] == second[l]){
                found = true;
                break;
            }
        }
        EXPECT_TRUE(found);
    }
}

void test::SmartVectorEqual(const std::vector<std::shared_ptr<std::vector<unsigned int>>>& first,
                            const std::vector<std::shared_ptr<std::vector<unsigned int>>>& second){
    unsigned int sizeFirst  = first.size();
    unsigned int sizeSecond = second.size();

    ASSERT_TRUE(sizeFirst  > 0);
    ASSERT_TRUE(sizeSecond > 0);
    ASSERT_EQ(sizeFirst, sizeSecond);

    for (unsigned int i = 0; i < sizeFirst; i++){
        const std::vector<unsigned int>& target = *first[i];
        unsigned int sizeTarget = target.size();
        ASSERT_TRUE(sizeTarget >= 2);
        bool found = false;
        for (unsigned int search = 0; search < sizeSecond; search++){
            const std::vector<unsigned int>& vec = *second[search];
            unsigned int sizeVec = vec.size();
            bool insideFound = false;
            for (unsigned int browse = 0; browse < sizeVec; browse++){
                if (target[0] == vec[browse] && target[1] == vec[(browse + 1) % sizeVec]){
                    insideFound = true;
                    ASSERT_TRUE(sizeVec >= 2);
                    ASSERT_EQ(sizeTarget, sizeVec);
                    for (unsigned int q = 0; q < sizeTarget; q++){
                        EXPECT_EQ(target[q], vec[(browse + q) % sizeVec]);
                    }
                    break;
                }
            }
            if (insideFound){
                found = true;
                break;
            }
        }
        EXPECT_TRUE(found);
    }
}
