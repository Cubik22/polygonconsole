#ifndef UTILSTEST_H
#define UTILSTEST_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include <math.h>
#include <exception>
#include <memory.h>

#include "Vector2f.h"
#include "Logger.h"
#include "Loader.h"
#include "Intersector.h"
#include "Polygon.h"
#include "Element.h"
#include "Mesh.h"


// ASSERT stops if not correct
// EXPECT continues if not correct

namespace test {

const std::vector<std::string>& GetPolygonsFileNames();

const std::vector<std::string>& GetPolygonsUnitTestsFileNames();

const std::vector<std::string>& GetElementsFileNames();

bool IsTestHard(const std::string& name);

std::vector<std::vector<Vector2f>> GetVerticesBorders(float width, float height, float borderThickX, float borderThickY);

bool AreDoubleEqualPercentage(double first, double second, double percentage);

bool LoadVerticesIndicesFromFile(std::vector<Vector2f>& vertices, std::vector<unsigned int>& indices, const std::string& _fileName);

bool LoadPolygonFromFile(std::vector<Vector2f>& vertices, std::vector<unsigned int>& indices,
                  std::vector<Vector2f>& segment,
                  std::vector<std::shared_ptr<std::vector<unsigned int>>>& polygonsIndices,
                  const std::string& _fileName);

bool LoadElementFromFile(std::vector<Vector2f>& vertices,
                         std::vector<std::shared_ptr<std::vector<unsigned int>>>& polygonsIndices,
                         const std::string& _fileName);

void SmartVectorEqual(const std::vector<Vector2f>& first, const std::vector<Vector2f>& second);

void SmartVectorEqual(const std::vector<std::shared_ptr<std::vector<unsigned int>>>& first,
                      const std::vector<std::shared_ptr<std::vector<unsigned int>>>& second);

}

#endif // UTILSTEST_H
