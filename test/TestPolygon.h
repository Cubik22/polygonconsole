#ifndef TESTPOLYGON_H
#define TESTPOLYGON_H

#include "UtilsTest.h"


namespace test {

TEST(TestPolygon, Create){
    const std::vector<Vector2f> vertices = { {1, 0}, {1, 1}, {2, 1} };
    const std::vector<unsigned int> indices = { 0, 1, 2, 3};

    Polygon poly = Polygon(vertices, indices);

    SmartVectorEqual(vertices, poly.getPoints());

    EXPECT_EQ(indices, poly.getIndices());

    // now we support the creation of polygons with different numbers of vertices and indices

//    std::vector<Vector2f> vertices = { {1, 0}, {1, 1}, {2, 1} };
//    std::vector<unsigned int> indices = { 0, 1, 2, 3};
//    try{
//        Polygon poly = Polygon(vertices, indices);
//        FAIL() << "expected runtime_error: 'Points vector and indices list should have the same size'";
//    } catch (const std::runtime_error& err){
//        EXPECT_EQ(err.what(), std::string("Points vector and indices list should have the same size"));
//    } catch (...){
//        FAIL() << "wrong exception, expected runtime_exception";
//    }
//    try{
//        Polygon poly;
//        poly.setBody(vertices, indices);
//        FAIL() << "expected runtime_error: 'Points vector and indices list should have the same size'";
//    } catch (const std::runtime_error& err){
//        EXPECT_EQ(err.what(), std::string("Points vector and indices list should have the same size"));
//    } catch (...){
//        FAIL() << "wrong exception, expected runtime_exception";
//    }
//    try{
//        indices.pop_back();
//        Polygon poly = Polygon(vertices, indices);
//        poly.getPoint(10);
//        FAIL() << "expected out_of_range: 'Array out of bound'";
//    } catch (const std::out_of_range& err){
//        EXPECT_EQ(err.what(), std::string("Array out of bound"));
//    } catch (...){
//        FAIL() << "wrong exception, expected out_of_range";
//    }
}

TEST(TestPolygon, Area){
    {
        std::vector<Vector2f> vertices = { {0, 0}, {1, 0}, {1, 1}, {0, 1} };
        std::vector<unsigned int> indices = { 0, 1, 2, 3 };

        EXPECT_EQ(1, Polygon::CalculateArea(vertices, indices));
    }
    {
        std::vector<Vector2f> vertices = { {-1, -1}, {1, -1}, {1, 1} };
        std::vector<unsigned int> indices = { 0, 1, 2 };

        EXPECT_EQ(2, Polygon::CalculateArea(vertices, indices));
    }
}

TEST(TestPolygon, Load){
    std::vector<Vector2f> vertices;
    std::vector<unsigned int> indices;
    std::vector<Vector2f> segment;
    std::vector<std::shared_ptr<std::vector<unsigned int>>> polygonsIndices;
    ASSERT_TRUE(LoadPolygonFromFile(vertices, indices, segment, polygonsIndices, "polygons/" + GetPolygonsFileNames()[0]));
}

TEST(TestPolygon, Cut){
    const std::vector<std::string> fileNames = GetPolygonsFileNames();
    for (const std::string& fileName : fileNames){
        Polygon polygon;
        std::vector<Vector2f> vertices;
        std::vector<unsigned int> indices;
        std::vector<Vector2f> segment;
        std::vector<std::shared_ptr<std::vector<unsigned int>>> polygonsIndicesCorrect;
        std::vector<std::shared_ptr<std::vector<unsigned int>>> polygonsIndicesCutted;
        ASSERT_TRUE(LoadPolygonFromFile(vertices, indices, segment, polygonsIndicesCorrect, "polygons/" + fileName));
        polygon.setBody(vertices, indices);
        polygon.setSegment(segment[0], segment[1]);
        polygon.createNetwork();
        polygonsIndicesCutted = polygon.cut();
        SmartVectorEqual(polygonsIndicesCorrect, polygonsIndicesCutted);
    }
}

TEST(TestPolygon, CutWithSegmentPointsAdded){
    const std::vector<std::string> fileNames = GetPolygonsUnitTestsFileNames();
    for (const std::string& fileName : fileNames){
        Polygon polygon;
        std::vector<Vector2f> vertices;
        std::vector<unsigned int> indices;
        std::vector<Vector2f> segment;
        std::vector<std::shared_ptr<std::vector<unsigned int>>> polygonsIndicesCorrect;
        std::vector<std::shared_ptr<std::vector<unsigned int>>> polygonsIndicesCutted;
        ASSERT_TRUE(LoadPolygonFromFile(vertices, indices, segment, polygonsIndicesCorrect, "unitTests/" + fileName));
        polygon.setBody(vertices, indices);
        polygon.setSegment(segment[0], segment[1]);
        polygon.createNetwork(true);
        polygonsIndicesCutted = polygon.cut();
//        std::cout << "correct:\n";
//        for (unsigned int i = 0; i < polygonsIndicesCorrect.size(); i++){
//            const std::vector<unsigned int>& indi = *polygonsIndicesCorrect[i];
//            for (unsigned int l = 0; l < indi.size(); l++){
//                std::cout << indi[l] << " ";
//            }
//            std::cout << "\n";
//        }
//        std::cout << "cutted:\n";
//        for (unsigned int i = 0; i < polygonsIndicesCutted.size(); i++){
//            const std::vector<unsigned int>& indi = *polygonsIndicesCutted[i];
//            for (unsigned int l = 0; l < indi.size(); l++){
//                std::cout << indi[l] << " ";
//            }
//            std::cout << "\n";
//        }
        SmartVectorEqual(polygonsIndicesCorrect, polygonsIndicesCutted);
    }
}

}

#endif // TESTPOLYGON_H
