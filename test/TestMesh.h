#ifndef TESTMESH_H
#define TESTMESH_H

#include "UtilsTest.h"

#define MAX_MESH_CONVEX 100
#define MAX_MESH_CONCAVE 50


namespace test {

TEST(TestMesh, Area){
    std::cout << "\nRunning mesh tests\n";
    std::cout << "This could take a while...\n" << std::endl;

    bool debug = false;
    bool writeSolutions = false;

    const std::vector<std::string> fileNames = GetElementsFileNames();

    std::vector<unsigned int> numbersConvex;
    std::vector<unsigned int> numbersConcave;
    for (unsigned int i = 2; i < MAX_MESH_CONVEX; i++){
        numbersConvex.push_back(i);
    }
    for (unsigned int i = 2; i < MAX_MESH_CONCAVE; i++){
        numbersConcave.push_back(i);
    }

    for (const std::string& fileName : fileNames){
        std::cout << "Testing " << fileName << "\n";

        std::vector<Vector2f> startVertices;
        std::vector<unsigned int> startIndices;
        EXPECT_TRUE(LoadVerticesIndicesFromFile(startVertices, startIndices, "polygons/" + fileName));

        Polygon poly = Polygon(startVertices, startIndices);
        Element element = Element(poly);
        element.createElement();

        float elementSize = 1.0f;
        float borderThick = 0.25f;

        unsigned int sizeVerticesBorders = 2;
        for (unsigned int bor = 0; bor < sizeVerticesBorders; bor++){
            bool concave = bor == 1;

            const std::vector<unsigned int>& numbers = concave ? numbersConcave : numbersConvex;

            for (const unsigned int& number : numbers){
                float lenght = number * elementSize;

                const std::vector<std::vector<Vector2f>> verticesBorders = GetVerticesBorders(lenght, lenght, borderThick, borderThick);

                const std::vector<Vector2f>& verticesBorder = verticesBorders[bor];

                Mesh mesh = Mesh(element, verticesBorder, number, number, elementSize, elementSize);

                std::vector<IndicesElement> indicesElement = concave ? mesh.cutConcave() : mesh.cut();

                unsigned int numberPolygons = 0;

                double areaInside  = 0;
                double areaOutside = 0;

                for (unsigned int i = 0; i < indicesElement.size(); i++){
                    const std::vector<Vector2f>& vertices = mesh.getVertices(i);
                    const std::vector<std::shared_ptr<std::vector<unsigned int>>>& insideIndices = *indicesElement[i].indicesInside;
                    const std::vector<std::shared_ptr<std::vector<unsigned int>>>& outsideIndices = *indicesElement[i].indicesOutside;
                    numberPolygons += insideIndices.size();
                    numberPolygons += outsideIndices.size();
                    if (debug){
                        std::cout << "Inside:\n";
                    }
                    for (unsigned int n = 0; n < insideIndices.size(); n++){
                        if (debug){
                            std::vector<unsigned int>& indi = *insideIndices[n];
                            for (unsigned int il = 0; il < indi.size(); il++){
                                std::cout << indi[il] << " ";
                            }
                            std::cout << "\n";
                        }
                        areaInside += Polygon::CalculateArea(vertices, *insideIndices[n]);
                    }
                    if (debug){
                        std::cout << "Outside:\n";
                    }
                    for (unsigned int n = 0; n < outsideIndices.size(); n++){
                        if (debug){
                            std::vector<unsigned int>& indi = *outsideIndices[n];
                            for (unsigned int il = 0; il < indi.size(); il++){
                                std::cout << indi[il] << " ";
                            }
                            std::cout << "\n";
                        }
                        areaOutside += Polygon::CalculateArea(vertices, *outsideIndices[n]);
                    }
                }

                if (debug){
                    std::cout << "number of polygons " << numberPolygons << "\n";
                }

                double areaMesh = areaInside + areaOutside;

                double correctAreaInside = Polygon::CalculateArea(verticesBorder);
                double correctAreaMesh = lenght * lenght;

                double toleranceAreaInside = concave ? 3*1.0E-2 : Vector2f::SOFT_TOLERANCE;
                bool resultAreaInside = AreDoubleEqualPercentage(correctAreaInside, areaInside, toleranceAreaInside);
                bool resultAreaMesh   = AreDoubleEqualPercentage(correctAreaMesh,   areaMesh,   Vector2f::SOFT_TOLERANCE);

                if (!resultAreaInside || !resultAreaMesh){
                    LOG(LogLevel::ERROR) << "file name: " << fileName << " number: " << number << " borders: " << bor;
                }
                if (!resultAreaInside || writeSolutions){
                    std::cout << "Correct    area inside: " << correctAreaInside << std::endl;
                    std::cout << "Calculated area inside: " << areaInside        << std::endl;
                }
                if (!resultAreaMesh || writeSolutions){
                    std::cout << "Correct    area mesh: " << correctAreaMesh << std::endl;
                    std::cout << "Calculated area mesh: " << areaMesh        << std::endl;
                }

                EXPECT_TRUE(resultAreaInside);
                EXPECT_TRUE(resultAreaMesh);
            }
        }
    }

    std::cout << std::endl;

//    std::cout << "Press something and then 'enter' to continue...\n";

//    std::string tmp;
//    std::cin >> tmp;
}

}

#endif // TESTMESH_H
