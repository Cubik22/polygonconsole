#ifndef TESTVECTOR2F_H
#define TESTVECTOR2F_H

#include "UtilsTest.h"


namespace test {

TEST(TestVector2f, Operations){
    Vector2f unoUno = {1, 1};
    Vector2f a = unoUno;
    Vector2f b = a;
    a.x = 2;
    a.y = 8;
    EXPECT_EQ(b, unoUno);
    EXPECT_EQ(a + b, Vector2f(3, 9));
    EXPECT_EQ(a - b, Vector2f(1, 7));
    EXPECT_EQ(b, unoUno);
}

TEST(TestVector2f, Dot){
    Vector2f a = { 0,  1};
    Vector2f b = { 0,  3};
    Vector2f c = { 1,  1};
    Vector2f d = { 0, -3};

    EXPECT_TRUE(Vector2f::AreDoublesEqual(a.dot(b),  3));
    EXPECT_TRUE(Vector2f::AreDoublesEqual(a.dot(c),  1));
    EXPECT_TRUE(Vector2f::AreDoublesEqual(a.dot(d), -3));
}

TEST(TestVector2f, Cross){
    Vector2f a = { 0,  1};
    Vector2f b = {-1,  0};
    Vector2f c = { 1,  0};
    Vector2f d = { 0,  3};

    EXPECT_TRUE(Vector2f::AreDoublesEqual(a.cross(b),  1));
    EXPECT_TRUE(Vector2f::AreDoublesEqual(a.cross(c), -1));
    EXPECT_TRUE(Vector2f::AreDoublesEqual(a.cross(d),  0));
}

TEST(TestVector2f, NormSquared){
    Vector2f a = { 0,  1};
    Vector2f b = { 0,  3};
    Vector2f c = { 1,  1};
    Vector2f d = { 0, -3};

    EXPECT_TRUE((a.normSquared() - 1) < Vector2f::TOLERANCE * Vector2f::TOLERANCE);
    EXPECT_TRUE((b.normSquared() - 9) < Vector2f::TOLERANCE * Vector2f::TOLERANCE);
    EXPECT_TRUE((c.normSquared() - 2) < Vector2f::TOLERANCE * Vector2f::TOLERANCE);
    EXPECT_TRUE((d.normSquared() - 9) < Vector2f::TOLERANCE * Vector2f::TOLERANCE);
}

}

#endif // TESTVECTOR2F_H
