#ifndef TESTINTERSECTOR_H
#define TESTINTERSECTOR_H

#include "UtilsTest.h"


namespace test {

TEST(TestIntersector, Intersection){
    Intersector inter;
    inter.setSegment1({ 0,  0}, { 0,  2});
    inter.setSegment2({ 1,  1}, { 5,  1});
    EXPECT_EQ(inter.calculateIntersection(false, false), IntersectionType::OutsideSegment);
    EXPECT_EQ(inter.calculateIntersection( true, false), IntersectionType::OutsideSegment);
    EXPECT_EQ(inter.calculateIntersection(false,  true), IntersectionType::InsideSegment);
    EXPECT_EQ(inter.calculateIntersection( true,  true), IntersectionType::InsideSegment);

    EXPECT_EQ(inter.getIntersectionPoint(), Vector2f(0, 1));

    inter.setSegment2({ 0,  2}, { 5,  2});
    IntersectionType result = inter.calculateIntersection(false, false);
    EXPECT_EQ(result, IntersectionType::BothOnVertex);

    inter.setSegment2({ 1,  0}, { 1,  2});
    EXPECT_EQ(inter.calculateIntersection(false, false), IntersectionType::Parallel);
}

TEST(TestIntersector, RelativePosition){
    Intersector inter;
    inter.setSegment1({ 0,  0}, { 1,  1});
    inter.setSegment2({ 0,  0}, {-1,  1});
    EXPECT_EQ(inter.calculateRelativePosition(), RelativePosition::Positive);

    inter.setSegment2({ 0,  0}, { 1, -1});
    EXPECT_EQ(inter.calculateRelativePosition(), RelativePosition::Negative);

    inter.setSegment2({ 1,  1}, { 2,  2});
    EXPECT_EQ(inter.calculateRelativePosition(), RelativePosition::Parallel);

    inter.setSegment2({ 0,  0}, { 1,  1});
    EXPECT_EQ(inter.calculateRelativePosition(), RelativePosition::Parallel);
}

}


#endif // TESTINTERSECTOR_H
