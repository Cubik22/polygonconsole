# segment
1.4 2.75
3.6 2.2
# number of vertices
5
# indices
0 1 2 3 4
# vertices
2.5 1.0
4.0 2.1
3.4 4.2
1.6 4.2
1.0 2.1
# number of small polygons
2
# small polygons indices
1 7 6 5 4 0
1 2 3 5 6 7
