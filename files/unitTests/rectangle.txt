# segment
2.0 1.2
4.0 3.0
# number of vertices
4
# indices
0 1 2 3
# vertices
1.0 1.0
5.0 1.0
5.0 3.1
1.0 3.1
# number of small polygons
2
# small polygons indices
0 4 6 7 5 3
4 1 2 5 7 6
